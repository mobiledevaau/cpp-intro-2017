{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Utilities\n",
    "\n",
    "* Today we will discuss a number of utilities in C++ which are handy in everyday work."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Resource management\n",
    "\n",
    "When we have a function e.g.\n",
    "\n",
    "```cpp\n",
    "int do_stuff()\n",
    "{\n",
    "    int a = do_other_stuff();\n",
    "    int b = a + 10;\n",
    "    return b;\n",
    "}\n",
    "```\n",
    "\n",
    "* The variables `a` and `b` are created on the stack. \n",
    "* You can think of the stack as a scratch pad where there function will store intermediate results. Once they are no longer needed they are automatically removed.\n",
    "* But what if we don't what to remove something?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Resource management\n",
    "\n",
    "We can create those objects on the *heap* or *free store*. \n",
    "\n",
    "* The *heap* is a segment of the address space reserved for dynamically allocated objects.\n",
    "* In C++ the raw way of (de)allocating memory is via\n",
    "  1. `new` and `delete`for normal objects\n",
    "  1. `new []` and `delete[]` for arrays of objects.\n",
    "\n",
    "Example:\n",
    "```cpp\n",
    "int* p = new int;\n",
    "int* pa = new int[2];\n",
    "\n",
    "*p = 0;\n",
    "pa[0] = 1;\n",
    "pa[1] = 1;\n",
    "\n",
    "delete p;\n",
    "delete [] pa;\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Resource management\n",
    "\n",
    "* The problem with manual memory management is that it \n",
    "  1. Is easy to get wrong\n",
    "  1. It clutters the code.\n",
    "  \n",
    "* In C++ we have *smart pointers* to help."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Smart pointers\n",
    "\n",
    "Smart pointers are designed to ensure proper resource management by using a principle called *Resource Allocation Is Initialization* (RAII). Example:\n",
    "\n",
    "```cpp\n",
    "struct buffer\n",
    "{\n",
    "    buffer(int size)\n",
    "    {\n",
    "        m_data = new char[size];\n",
    "    }\n",
    "    ~buffer()\n",
    "    {\n",
    "        delete [] m_data;\n",
    "    }\n",
    "    \n",
    "    char* m_data;\n",
    "};\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Smart pointers\n",
    "\n",
    "Usage:\n",
    "```cpp\n",
    "{\n",
    "    buffer b(100);\n",
    "    if(...)\n",
    "       return; // <- Exit\n",
    "       \n",
    "    if(i > 3)\n",
    "       return; // <- Exit\n",
    "       \n",
    "    // Exit\n",
    "}\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Smart pointers (`std::shared_ptr`)\n",
    "\n",
    "Shared ownership of objects."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```cpp\n",
    "#include <memory>\n",
    "#include <iostream>\n",
    "\n",
    "int main()\n",
    "{\n",
    "    std::shared_ptr<int> p(new int);    // ref count = 1\n",
    "    *p = 32;\n",
    "    std::cout << *p << std::endl;\n",
    "\n",
    "    {\n",
    "        std::shared_ptr<int> x = p;     // ref count = 2\n",
    "        {\n",
    "            std::shared_ptr<int> y = x; // ref count = 3\n",
    "        }                               // ref count = 2\n",
    "    }                                   // ref count = 1\n",
    "\n",
    "    return 0;                           // ref count = 0, destory p\n",
    "}\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Smart pointers (`std::shared_ptr`)\n",
    "\n",
    "* Extra helpers `std::weak_ptr` breaks ownership cycles:\n",
    "\n",
    "```cpp\n",
    "struct a { std::shared_ptr<b> m_b; };\n",
    "struct b { std::shared_ptr<a> m_a; };\n",
    "\n",
    "std::shared_ptr<a> pa(new a);\n",
    "std::shared_ptr<b> pb(new b);\n",
    "pa->m_b = pb;\n",
    "pb->m_a = pa; // Woops likely memory leak\n",
    "```\n",
    "\n",
    "* More efficiency `std::make_shared`:\n",
    "\n",
    "```cpp\n",
    "std::shared_ptr<a> pa = std::make_shared<a>();\n",
    "std::shared_ptr<b> pb = std::make_shared<b>();\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Smart pointers (`std::unique_ptr`)\n",
    "\n",
    "Unique ownership of objects."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```cpp\n",
    "#include <memory>\n",
    "\n",
    "int main()\n",
    "{\n",
    "    std::unique_ptr<int> p1(new int);\n",
    "    std::unique_ptr<int> p2 = p1;\n",
    "}\n",
    "```\n",
    "\n",
    "Error:\n",
    "\n",
    "```\n",
    "'/usr/include/c++/4.9/bits/unique_ptr.h:356:7: note: declared here',\n",
    " '       unique_ptr(const unique_ptr&) = delete;',\n",
    " '       ^']\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Smart pointers (`std::unique_ptr`)\n",
    "\n",
    "Move ownership of `unique_ptr`\n",
    "\n",
    "```cpp\n",
    "#include <memory>\n",
    "\n",
    "int main()\n",
    "{\n",
    "    std::unique_ptr<int> p1(new int);\n",
    "    std::unique_ptr<int> p2 = std::move(p1); // p1 is no longer valid\n",
    "}\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Smart pointers (`std::shared_ptr`)\n",
    "\n",
    "* Hands-on: \n",
    "    * Wrap the following allocation in a `std::shared_ptr<object>`\n",
    "* No working compiler: go to http://ideone.com\n",
    "* Time 5 min\n",
    "\n",
    "```\n",
    "#include <iostream>\n",
    "#include <memory>\n",
    "\n",
    "struct object\n",
    "{\n",
    "    object(){ std::cout << \"object()\" << std::endl; }\n",
    "    ~object(){ std::cout << \"~object()\" << std::endl; }\n",
    "};\n",
    "\n",
    "int main()\n",
    "{\n",
    "    object* o = new object();\n",
    "}\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "```cpp\n",
    "#include <iostream>\n",
    "#include <memory>\n",
    "\n",
    "struct object\n",
    "{\n",
    "    object(){ std::cout << \"object()\" << std::endl; }\n",
    "    ~object(){ std::cout << \"~object()\" << std::endl; }\n",
    "};\n",
    "\n",
    "int main()\n",
    "{\n",
    "    std::shared_ptr<object> o = std::make_shared<object>();\n",
    "}\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Random number generators\n",
    "\n",
    "In C++ we now have replacements for the C functions `srand()` and `rand()`:\n",
    "\n",
    "* Random number engines. \n",
    "* Random number distributions (e.g. uniform, normal, or poisson distributions)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "```cpp\n",
    "#include <iostream>\n",
    "#include <string>\n",
    "#include <map>\n",
    "#include <random>\n",
    " \n",
    "/// From: http://en.cppreference.com/w/cpp/numeric/random/random_device    \n",
    "int main()\n",
    "{\n",
    "    std::random_device rd;\n",
    "    std::map<int, int> hist;\n",
    "    std::uniform_int_distribution<int> dist(0, 4);\n",
    "    for (int n = 0; n < 20000; ++n) {\n",
    "        ++hist[dist(rd)]; // note: demo only: the performance of many \n",
    "                          // implementations of random_device degrades sharply\n",
    "                          // once the entropy pool is exhausted. For practical use\n",
    "                          // random_device is generally only used to seed \n",
    "                          // a PRNG such as mt19937\n",
    "    }\n",
    "    for (auto p : hist) {\n",
    "        std::cout << p.first << \" : \" << std::string(p.second/100, '*') << '\\n';\n",
    "    }\n",
    "}\n",
    "```\n",
    "\n",
    "Output:\n",
    "```\n",
    "['0 : ***************************************',\n",
    " '1 : ****************************************',\n",
    " '2 : ****************************************',\n",
    " '3 : ***************************************',\n",
    " '4 : ***************************************']\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Random number generators"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<iframe src=http://en.cppreference.com/w/cpp/numeric/random width=800 height=600></iframe>"
      ],
      "text/plain": [
       "<IPython.core.display.HTML object>"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from IPython.display import HTML\n",
    "HTML('<iframe src=http://en.cppreference.com/w/cpp/numeric/random width=800 height=600></iframe>')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Regular expressions\n",
    "Regular expressions are typically used to match patterns in text, and in C++11 we have this functionality baked in."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```cpp\n",
    "#include <iostream>\n",
    "#include <regex>\n",
    "#include <string>\n",
    "\n",
    "int main()\n",
    "{\n",
    "    std::string input1(\"+500\");\n",
    "    std::string input2(\"-2\");\n",
    "    std::string input3(\"42hello\");\n",
    "\n",
    "    std::regex integer(R\"((\\+|-)?[[:digit:]]+)\");\n",
    "\n",
    "    std::cout << \"match input1 = \" << std::regex_match(input1, integer) << std::endl;\n",
    "    std::cout << \"match input2 = \" << std::regex_match(input2, integer) << std::endl;\n",
    "    std::cout << \"match input3 = \" << std::regex_match(input3, integer) << std::endl;\n",
    "}\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Time tracking utilities `<chrono>`\n",
    "\n",
    "The chrono library provides various functionality to deal with and measure time.\n",
    "\n",
    "* The library defines three main types as well as utility functions and common typedefs.\n",
    "    * clocks: Different resolution / property clocks for getting time\n",
    "    * time points: A point in time.\n",
    "    * durations: Subtracting two time points gives a duration.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Time tracking utilities `<chrono>`\n",
    "\n",
    "```cpp\n",
    "#include <iostream>\n",
    "#include <chrono>\n",
    "\n",
    "long fibonacci(unsigned n)\n",
    "{\n",
    "    if (n < 2) return n;\n",
    "    return fibonacci(n-1) + fibonacci(n-2);\n",
    "}\n",
    "\n",
    "int main()\n",
    "{\n",
    "    auto start = std::chrono::high_resolution_clock::now();\n",
    "    std::cout << \"fibonacci(42) = \" << fibonacci(42) << std::endl;\n",
    "    auto end = std::chrono::high_resolution_clock::now();\n",
    "\n",
    "    auto result = std::chrono::duration_cast<std::chrono::microseconds>(end-start);\n",
    "    std::cout << \"Time (ms) = \" << result.count() << std::endl;\n",
    "}\n",
    "\n",
    "Output:\n",
    "    \n",
    "```\n",
    "['fibonacci(42) = 267914296', 'Time (ms) = 2034064']\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Time tracking utilities `<chrono>`\n",
    "\n",
    "* Hands-on: Which one is fastest for n = 1000?\n",
    "* No working compiler: go to http://ideone.com\n",
    "* Time 10 min\n",
    "\n",
    "```cpp\n",
    "#include <iostream>\n",
    "#include <chrono>\n",
    "#include <vector>\n",
    "\n",
    "std::vector<int> fill_v1(int n)\n",
    "{\n",
    "    std::vector<int> v;\n",
    "    // v.reserve(n); \n",
    "    for(int i = 0; i < n; ++i) v.push_back(i);\n",
    "    return v;\n",
    "}\n",
    "\n",
    "int main()\n",
    "{\n",
    "    auto start = std::chrono::high_resolution_clock::now();\n",
    "    // Run code\n",
    "    auto end = std::chrono::high_resolution_clock::now();\n",
    "    auto result = std::chrono::duration_cast<std::chrono::microseconds>(end-start);\n",
    "    std::cout << \"Time (ms) = \" << result.count() << std::endl;\n",
    "}\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Possible solution part 1/2\n",
    "\n",
    "```cpp\n",
    "#include <iostream>\n",
    "#include <chrono>\n",
    "#include <vector>\n",
    "#include <functional>\n",
    "\n",
    "std::vector<int> fill_v1(int n)\n",
    "{\n",
    "    std::vector<int> v;\n",
    "    for(int i = 0; i < n; ++i) v.push_back(i);\n",
    "    return v;\n",
    "}\n",
    "\n",
    "std::vector<int> fill_v2(int n)\n",
    "{\n",
    "    std::vector<int> v;\n",
    "    v.reserve(n);\n",
    "    for(int i = 0; i < n; ++i) v[i] = i;\n",
    "    return v;\n",
    "}\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Possible solution part 2/2\n",
    "```cpp\n",
    "std::vector<int> run(std::function<std::vector<int>()> function)\n",
    "{\n",
    "    auto start = std::chrono::high_resolution_clock::now();\n",
    "    auto r = function();\n",
    "    auto end = std::chrono::high_resolution_clock::now();\n",
    "    auto result = std::chrono::duration_cast<std::chrono::microseconds>(end-start);\n",
    "    std::cout << \"Time (ms) = \" << result.count() << std::endl;\n",
    "    return r;\n",
    "}\n",
    "\n",
    "int main()\n",
    "{\n",
    "    auto r1 = run(std::bind(fill_v1, 1000));\n",
    "    auto r2 = run(std::bind(fill_v2, 1000));\n",
    "}\n",
    "```\n",
    "\n",
    "Output:\n",
    "    \n",
    "```\n",
    "['Time (ms) = 52', 'Time (ms) = 6']\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Threading facilities `<thread>`\n",
    "\n",
    "In C++ we also have standard threading and syncronization support via ``std::thread``, ``std::mutex``, ``std::condition_variable`` etc. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```cpp\n",
    "#include <iostream>\n",
    "#include <thread>\n",
    "#include <vector>\n",
    "\n",
    "void do_work()\n",
    "{\n",
    "    std::cout << \"thread \" << std::this_thread::get_id() << \" sleeping...\\n\";\n",
    "    std::this_thread::sleep_for(std::chrono::seconds(1));\n",
    "}\n",
    "\n",
    "int main()\n",
    "{\n",
    "    std::thread t1(do_work);\n",
    "    std::thread t2(do_work);\n",
    " \n",
    "    t1.join();\n",
    "    t2.join();\n",
    "}\n",
    "```"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
