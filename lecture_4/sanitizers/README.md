# Sanitizers

* Sanitizers are compiler tools that do run-time analysis of our code to
    find various problems.
  * Both `clang` and recent versions of `gcc` have sanitizer support.

 # Sanitizers

 * `clang` currently has the most extensive support. There are different
 types of sanitizers to see all of them Google the clang documentation.

 Let look at a few for finding bugs in our code.

 1. AddressSanitizer: Can find out of bounds errors, use after free etc.
 1. ThreadSanitizer: Finds e.g. data races.
 1. MemorySanitizer: Find e.g. uninitialized reads.
 1. UndefinedBehaviorSanitizer: Find e.g. misaligned data access,
    integer overflow
 1. LeakSanitizer: Runtime memory leak detector.

 # Example:

 ```
 // this causes a stack-buffer-overflow
 #include <iostream>

 int main()
 {
   char array[] {"hello world"};

   for (int i = 0; i <= 12; ++i)
   {
       std::cout << array[i];
   }

   return 0;
 }

```

`clang++ -std=c++14 -fsanitize=address -g -O1 main.cpp`

# Valgrind

*Dynamic analysis tool* for cheching that your application performs correctly.

Here are some of the things Valgrind can find:

- Off by-one errors (e.g. looping too far).
- Invalid memory access (e.g. accessing memory you have not allocated)

Supported platforms primarily include:
1. A varity of Linux (including Android).
2. Mac OSX

Major advantage over the also popular sanitizers is that it requires no changes to the source code or compilation of you program. You can take a program that you've already build and then run it using valgrind.

Major drawback is that ``valgrind`` is quite a bit slower than e.g. the ``clang`` sanitizers.
