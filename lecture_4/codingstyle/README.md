# Coding style

* Coding style is a subjective topic where individual taste and preferences
  are the major issue.

* Big projects typically have their own coding style
    * Google
    * Mozilla https://developer.mozilla.org/en-US/docs/Mozilla/Developer_guide/Coding_Style

## Tooling

* Maintaining a specific coding style by hand is tedious. So instead it
  is often more efficient to use a tool for it.

  * `clang-format`:
  * `astyle`:http://astyle.sourceforge.net/astyle.html
  * `uncrustify`:

## Demo

Run with `clang-format -i mess.cpp`

```
#include <algorithm>
#include <cstdlib>
#include <ctime>
#include <functional>
#include <iostream>
#include <iterator>

template <typename T, int size> bool is_sorted(T (&array)[size]) {
  return std::adjacent_find(array, array + size, std::greater<T>()) ==
         array + size;
}

int main() {
  std::srand(std::time(0));

  int list[] = {1,
  2,
  3,
  4,
  5,
  6,
  7,
  8,
  9};

					  do {
					    std::random_shuffle(list, list + 9);
					  } while (is_sorted(list));

  int score = 0;

  do
  {

    std::cout << "Current list: ";



    std::copy(
    	list, list + 9, std::ostream_iterator<int>(std::cout, " "));

    int rev;
    while (true) {
      std::cout
      << "\nDigits to reverse? ";
      std::cin >> rev;
      if (rev > 1 && rev < 10)
        break;
      std::cout
      << "Please enter a value between 2 and 9.";
    }

    ++score;
    std::reverse(list, list + rev);
  } while (!is_sorted(list));

  std::cout << "Congratulations, you sorted the list.\n"
            << "You needed " << score << " reversals." << std::endl;
  return 0;
}
```
