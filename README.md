# Home of the course modern C++ introduction. 

On this repository you will find the wiki containing information about the
course and in addition we will make available slides and other material
relevant.

* Go to the
  [wiki](https://bitbucket.org/mobiledevaau/cpp-intro-2017/wiki) for
  an overview.