# Overview

The C++ coding session for library overview lecture 3.

## Library Overview

*  In the previous sessions we've seen some of the standard library
  * Such as `std::string`, `std::ostream`
* There are lots of core functionality there to explore - prefer
  it over your own.
* The C++ standard library does not contain things like GUI,
  database, network functionality. *At least not yet*.

## Library Overview - the components

* Run-time language support (e.g., for allocation and run-time type information).
* The C standard library (with very minor modifications to minimize violations of the type system).
* Strings (with support for international character sets and localization).

## Library Overview - the components

* Support for regular expression matching.
* I/O streams is an extensible framework for input and output to
  which users can add their own types, streams, buffering
  strategies, locales, and character sets.
* A framework of containers (such as `vector` and `map` ) and
  algorithms (such as `find()` , `sort()` , and `merge()` )

## Library Overview - the components

* Support for numerical computation (such as standard
  mathematical functions `sqrt()`, `log()`, complex numbers,
  and random number generators)

Support for concurrent programming, including threads and locks; see Chapter 13 . The concurrency support is foundational so that users can add support for new models of concurrency as libraries. • Utilities to support template metaprogramming (e.g., type traits; § 11.6 ), STL-style generic programming (e.g., pair ; § 11.3.3 ), and general programming (e.g., clock ; § 11.4 ). • “Smart pointers” for resource management (e.g., unique_ptr and shared_ptr ; § 11.2.1 ) and an interface to garbage collectors (§ 4.6.4 ). • Special-purpose containers, such as array (§ 11.3.1 ), bitset (§ 11.3.2 ), and tuple (§ 11.3.3 ).

# Delete
        :::cpp
        [](auto v){ std::cout << v << "\n"; }
