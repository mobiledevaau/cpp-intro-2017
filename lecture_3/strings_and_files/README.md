# Overview

C++ Strings and files


1. The C++ standard library offers a string type to save
  most users from C-style manipulation of arrays of characters
  through pointers.

        :::cpp
        #include <string>

        std::string h1 {"Hello "};

        // Or using string litterals
        using namespace std::string_literals;
        auto h2 = "world"s
        std::cout << h1 << h2;

1. Mention `using` with namespaces
1. We can also create string from numbers `std::to_string`
1. Raw strings

        :::cpp
        // Tedious escaping
        std::string path("C:\\A\\B\\C\\test.txt")

        // No tedious escaping
        std::string path(R"(C:\A\B\C\test.txt)")

        // Fail without delimitors
        // auto p = R"(C:)";\)";
        auto p = R"ok(C:)";\)ok";

1. The `std::string` type provides a number of useful functions
   such as size and concatenation. Visit the
   http://en.cppreference.com/w/cpp/string

1. Example of concatenation:

        :::cpp
        std::string join(const std::string& name, const std::string& domain)
        {
          return name + '@' + domain;
        }

1. Hands on: Use the `join()` function to print your AAU email.

        :::cpp
        #include <string>
        #include <iostream>

        std::string join(const std::string& name, const std::string& domain)
        {
            return name + '@' + domain;
        }

        int main()
        {
            std::cout << join("mvp", "es.aau.dk") << "\n";
            return 0;
        }

1. You can use the different string operations to manipulate the strings.
   However, C++ also supports regular expressions (regex). We are not
   going to learn regex here, but lets have a quick look.

        :::cpp
        auto email = join("mvp", "es.aau.dk");

        std::regex pattern(R"((.+)@(.+))");

        std::smatch match;

        std::regex_search(email, match, pattern);

        std::cout << "Found " << match.size() << " matches\n";
        for (auto& i : match)
        {
            std::cout << i.str() << "\n";
        }

1. We've seen how to print data to the screen. What if we want to write
   to a file?
   In `<fstream>`, the standard library provides streams to and from
   a file:

    1. ifstream s for reading from a file
    1. ofstream s for writing to a file
    1. fstream s for reading from and writing to a file

    For example:

        :::cpp
        std::ofstream out("my_email.txt");
        out << "mvp@es.aau.dk";

1. We can check for things on the file e.g. that it is operations

        :::cpp
        assert(out.is_open());

    1. A side: mention `cassert`

1. We can also read the file back with `ifstream`:

   For example:

        :::cpp
        std::ifstream in("my_email.txt");
        std::string email;
        in >> email;

1. Lets say we have multiple lines:

        :::cpp
        std::ifstream in("my_email.txt");
        std::string m;

        while(std::getline(in, m))  
        {
            std::cout << m;
        }

1. If you work with very big files you should consider memory mapped io

1. File system TS:
   `g++ -std=c++17 main.cpp -lstdc++fs`

        :::cpp
        #include <experimental/filesystem>

        namespace fs = std::experimental::filesystem;

        int main()
        {
           for(auto& p: fs::directory_iterator(fs::current_path()))
               std::cout << p << '\n';
           return 0;
        }
