# Overview

C++ Containers


1. Most programs we write will need to work on / store collections of
   data. We already saw this with `std::string` which stores a collection
   of characters.
1. If the purpose of a type is to store and provide access to some other
   objects we typically refer to it as a container.  
1. Lets consider a simple example of implementing a todo list. We will
   need a container to store our todo items.

        :::cpp

        struct todo_item
        {
           std::string m_item;
           std::string m_category;
        };

        std::ostream& operator<<(std::ostream& out, const todo_item& item)
        {
           out << "todo_item: m_item = " << item.m_item
               << " m_category = " << item.m_category;

           return out;
        }

        int main()
        {
           todo_item item1{"Do homework", "work"};
           todo_item item2{"Call insurance", "personal"};

           std::cout << item1 << "\n";
           std::cout << item2 << "\n";

           return 0;
        }
        
1. One of the most popular containers in C++ is the `std::vector`. Lets
   see how to use it.

        :::cpp
        std::vector<todo_item> items {
           {"Do homework", "work"},
           {"Call insurance", "personal"}
        };

        for (auto& item : items)
        {
           std::cout << item << "\n";
        }

1. We can add more items:

        :::cpp
        void add_item(std::vector<todo_item>& items)
        {
            todo_item item;
            std::cout << "Enter item text:\n";
            std::getline(std::cin, item.m_item);
            std::cout << "Enter item category:\n";
            std::getline(std::cin, item.m_category);
            items.push_back(item);
        }

1. Hands on: implement a function that only prints items with a
   certain category. Hint: you can use a for loop to iterate
   over all elements and use == operator to see if two strings
   are equal.

        :::cpp
        void print_category(const std::vector<todo_item>& items)
        {
           std::string category;
           std::cout << "Enter item category:\n";
           std::getline(std::cin, category);

           for (auto& item : items)
           {
               if (item.m_category == category)
               {
                   std::cout << item << "\n";
               }
           }
        }

1. Show that a vector has a size and index operator
1. Show `std::vector` on cppreference

1. Switch to map (phonebook example), here we have key, value
    1. mention `std::pair` is used for the entries

            :::cpp
            std::map<std::string, int> phonebook {
                { "alice", 234 },
                { "bob", 543 }
            };

            for (auto& item : phonebook)
            {
                std::cout << item.first << " " << item.second << "\n";
            }

    1. Show how to insert a entry

            :::cpp
            phonebook.insert({"Eve", 876});

    1. We can access by key lookup

            :::cpp
            std::cout << "By key = " << phonebook["bob"] << std::endl;

1. Discuss: wrong key:

        :::cpp
        std::cout << "By key = " << phonebook.at("bo2b") << std::endl;

1. Discuss: Program terminated

1. A side: Exceptions

        :::cpp
        try
        {
            std::cout << "By key = " << phonebook.at("bo2b") << std::endl;
        }
        catch (const std::out_of_range& e)
        {
            std::cout << "Not found";
            std::cout << e.what();
        }

1. Hands on: Implement the `std::map` make a function which takes user
   input

1. Show container overview http://www.cplusplus.com/reference/stl/
1. Done
