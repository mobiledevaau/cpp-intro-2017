# Overview

The C++ coding session for lecture 2.

## Classes

1. Lets create a simple class:

        ```
        class lecture
        {
        public:
           const char* date() const
           {
               return "15th nov. 2017";
           }

        };
        ```

    1. Discuss access specifiers and `const` members.
    1. Mention `struct`
    1. Show that we can also define the function date outside the class.
    1. Discuss that typically we put our class in a `.hpp` header and the
       member function definitions in the `.cpp` file.
1. Aside: Use `std::string`
    1. Discuss return by `pointer`, `value`, `reference`.
1. Hands on: Add a location member function.
1. Add two member variables `m_date` and `m_location`
1. Initialize the two members in a constructor.
    1. Also make the destructor.
    1. Discuss the purpose of constructor / destructor
1. Hands on: Add arguments to the constructor and add two prints to the
 constructor and and destructor. See when the object is constructed and
 destroyed:

        ```
        std::cout << "before scope\n";
        {
            // .. code here
        }
        std::cout << "after scope\n";
        ```

1. Add the output operator. When you see `a @ b;` This can be implemented
 in two ways:
    1. As a member function:

            ```
            class A
            {
              void operator@(B& b);
            };
            ```

    1. As a free function:

            ```
            void operator@(A& a, B& b);
            ```

1. Create an output operator for our `lecture` class (only printing
    `date()`)
    1. Hands on: Also add a print for location.

1. Abstract types; what we have seen so far is called concrete types
   because we have implementation and interface together. In an Abstract
   type we can separate interface and implementation. So for the same
   interface we can have many implementations

        ```
        class lecture
        {
        public:
           virtual bool take_exam() const = 0;
        };

        class math_lecture : public lecture
        {
        public:
           bool take_exam() const override
           {
               std::cout << "What is 2 + 2" << std::endl;
               int answer;
               std::cin >> answer;

               return answer == 4;
           }
        };
        ```
    1. Mention: Inheritance (base class, derived class)
    1. Mention: Pure vs. non-pure virtual
    1. Mention: Override keyword
    1. Mention: `vtbl` so the virtual function pointer table makes sure
     +the right function is called. What about construction / destruction
1. Now we can make functions that only use the interface.

       ```
       void use(lecture& l)
       {
           if (l.take_exam())
           {
               std::cout << "Good job!\n";
           }
           else
           {
               std::cout << "Ohhh no\n";
           }
       }
       ```
   1. Can unfortunately only be used via references or pointers.
1. Hands on: Write a c++ lecture which asks the question:

       ```
       std::cout << "What year was the latest C++ standard released" << std::endl;
       ```

1. Abstract type and inheritance are the corner-stone of OOP programming.

1. We can also provide default implementations in the base class:

        ```
        class lecture
        {
        public:
           virtual bool take_exam() const = 0;
           virtual std::string city() { return "Aalborg"; }
        };
        ```
1. More constructors, in some cases it makes sense to have multiple
 different constructors. Say we have the following:

        ```
        class thing
        {
        public:

           thing(int a, int b)
               : m_a(a), m_b(b)
               {
                   m_avg = (m_a + m_b) / 2;
               }

           int m_a;
           int m_b;
           int m_avg;
        };
        ```

 And we want allow only argument `a` to be specified. We can add a
 second constructor:

        ```
        thing(int a)
           : m_a(a), m_b(0)
           {
               m_avg = (m_a + m_b) / 2;
           }
        ```

 However, now have two implementations for `m_avg`. We can also
 delegate constructors (i.e. reuse them):

        ```
        thing(int a)
            : thing(a, 0)
            { }
        ```

1. We can also copy objects. `thing a; thing b(a);` This is done via
 the copy constructor. In many cases it can be trivial.
    1. In the trivial case the copy constructor is generated for us.
    1. Hands on: Copy one object and print it's values to check it
     actually works.
    1. But in some cases we need to take special care:

            ```
            thing(const thing& t)
               : thing(t.m_a, t.m_b)
               {
                   std::cout << "copy\n";
               }
            ```

    1. We can also implement copy assignment:

            ```
            thing& operator=(const thing& t)
            {
               // Check for self-assignment via this pointer
               m_a = t.m_a;
               m_b = t.m_b;
               m_avg = t.m_avg;
               std::cout << "assign\n";
            }
            ```

    1. Finally we can also implement an r-value constructor

            ```
            thing(thing&& t)
               : thing(t.m_a, t.m_b)
               {
                   std::cout << "move\n";
               }
            ```

1. Done
