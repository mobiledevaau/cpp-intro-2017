# Overview

The C++ coding session for templates lecture 2.

## Templates

1. Start with a basic `int sum(int a, int b)` function
1. What if we also want our function to work with other types e.g.
 `double`.
1. Show template version of `T sum(T a, T b)`
    1. Notice type deduction for the function arguments
    1. We can also explicitly specify them at the call site.
1. Show template version with different input `auto sum(T a, U b)` types
 and `auto` return.
1. Hands on: Implement subtract function.
1. Aside: Show that you can also use return type deduction with
   non-template functions.
1. Show specialization for `T sum(T a, T b)`.
    1. First with template

            :::cpp
            template<>
            auto sum<double, double>(double a, double b)
            {
                std::cout << "Special\n";
                return a + b;
            }


    1. Then show that basic overload is possible
     `duble sum(double a, double b)`
    1. Mention partial and full specialization.
1. Hands on: Specialize subtract for `double`
1. Lets say we want our sum function to accept multiple parameters.
    1. Show version with recursion
    1. Show version with fold expressions::

            ( pack op ... )	(1)	(since C++17)
            ( ... op pack )	(2)	(since C++17)
            ( pack op ... op init )	(3)	(since C++17)
            ( init op ... op pack )	(4)	(since C++17)

1. Hands on: Implement subtract function.
1. We can also have class templates

        :::cpp
        template<class P>
        class point
        {
        public:
            point(P x, P y)
                : m_x(x), m_y(y)
            { }

            P m_x;
            P m_y;
        };

        point<int> p1(2,3);
        point<double> p2(2.2, 3.2);

1. Create an `operator<<` that works for `int`:

        :::cpp
        std::ostream& operator<<(std::ostream& out, point<int>& p)
        {
            return out << "{" << p.m_x << "," << p.m_y << "}";
        }

    1. Show that it does not work for double
    1. Hands on: Implement the version for `double`
    1. Make it a template.
    1. Hands on: Make yours a template.
1. Show type deduction for class template parameters (c++17):

        :::cpp
        point p1(2,3);
        point p2(2.2, 3.2);

    1. Discuss with initialization this is not possible.

1. Introduce `std::vector<T>`:

        :::cpp
        #include <iostream>
        #include <vector>

        int main()
        {
            std::vector<int> v = {1,2,3};
            return 0;
        }

    1. Can I write this in a simpler way? (think deduction guide)

1. Function objects.
    1. Function objects are objects that behave like functions. They
       do so my overloading the call operator. They are useful since
       the allow us to keep state:

            :::cpp
            class printer
            {
            public:
                void operator()()
                {
                    std::cout << "hello c++\n";
                    ++m_calls;
                }
                uint32_t m_calls = 0;
            };

            printer print;
            print();
            print();
            std::cout << print.m_calls;

    1. Another use of functors is as template arguments to generic
       function. E.g. we can make code like:

            :::cpp
            template<class Operation>
            void go(std::vector<int>& values, Operation op)
            {
               for(auto& v : values)
               {
                   op(v);
               }
            }

            struct printer
            {
               void operator()(int v){ std::cout << v << "\n"; }
            };

    1. Hands on: Make a functor that will sum the values in the vector.
    1. We just re-invented part of the standard library.
        1. http://en.cppreference.com/w/cpp/algorithm/for_each
        1. `<algorithm>` contains a lot of good stuff.
    1. So we can write:

            :::cpp
            struct printer
            {
                void operator()(int v){ std::cout << v << "\n"; }
            };

            std::vector v{1,2,3,4,2};
            printer print;

            std::for_each(std::begin(v), std::end(v), print);

    1. Mention: ADL i.e. we don't have to use `std::` but maybe it is
       best.
    1. We can make our functor's `operator()(X x)` a template to work
       with any value.

1. Lambdas, in C++ we also have the ability to create anonymous
 functions, which can be used in a lot of places where could also use
 a functor.

 The three most common used version are:

        [ capture-list ] ( params ) -> ret { body }
        [ capture-list ] ( params ) { body }
        [ capture-list ] { body }

 Lets take our printer example and implement it with a lambda:

        :::cpp
        std::for_each(std::begin(v), std::end(v),
           [](int v){ std::cout << v << "\n"; });

   1. The `capture-list` section allows us to make variables external
    variable available inside the scope of the lambda.
   1. The `parms` are simply the arguments the lambda accepts (may
    be omitted if now arguments).
   1. `ret` specifies the return type of the lambda (may be omitted
    if void or single return).
   1. `body` contains the functionality of the lambda
1. Hands on: Implement a lambda.
1. Lambda `capture-list` can be used to capture external variables
   in a number of ways:

        :::cpp
        []        //no variables defined. Attempting to use any external variables in the lambda is an error.
        [x, &y]   //x is captured by value, y is captured by reference
        [&]       //any external variable is implicitly captured by reference if used
        [=]       //any external variable is implicitly captured by value if used
        [&, x]    //x is explicitly captured by value. Other variables will be captured by reference
        [=, &z]   //z is explicitly captured by reference. Other variables will be captured by value

1. Hands on: Try to implement the sum functor as a lambda.
    1. Example:

            :::cpp
            int sum = 0;

            std::for_each(std::begin(v), std::end(v),
                [&sum](int v){ sum += v; });

1. Generic lambdas, our lambda functions can also behave like template
   function or functor.

        :::cpp
        [](auto v){ std::cout << v << "\n"; }

1. Done
